import React from 'react';
import './carrito.css';
import Producto from './Producto';

const Carrito = ({carrito, agregarProducto}) => ( 
        <div className="carrito" >
            <h2>Carrito de compras</h2>
        {/* Si el chango está vacío, meto mensaje */}
        {carrito.length === 0
        ?
            <p>El changuito está vacío</p>
        :
        carrito.map(producto => (
            <Producto 
                key={producto.id}
                producto={producto}
                carrito={carrito}
                agregarProducto={agregarProducto}
            />
            ))
        }
        </div>
      );
 
export default Carrito;