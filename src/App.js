import React, {Fragment, useState} from 'react';
import Header from './components/Header'; 
import Footer from './components/Footer'; 
import Producto from './components/Producto'; 
import Carrito from './components/Carrito';

function App() {

// Dijimos que este es un buen lugar para poner Javascript.
// Hicimos ejemplos simples 

const universidad = "Unahur";

let mensaje;

if (universidad === "Unahur") {
  mensaje = "Librería Artística Caravaggio"
} else {
  mensaje = "No podés entrar"
};

// Ahora vamos con un listado de productos
// Fijarse en la línea 1, importo useState
// Defino un estado inicial
// La sintaxis es 
// const [nombre_del_state, función que lo reescribe] = useState("valores iniciales") 
// La función gralmente se denomina setNombre

const [ productos, setProductos] = useState([
  {id:0, articulo:"Atril de madera", precio:4500},
  {id:1, articulo:"Acuarela Van Gogh", precio:500},
  {id:2, articulo:"Acuarela Cotman", precio:350},
  {id:3, articulo:"Papel Arches / M", precio:2300},
  {id:4, articulo:"Kit regalo W & N", precio:12000},
]
)

// State para un carrito de compras
// en este caso, obviamente, se inicia vacío.
// Recordar, en la definición del state, va primero el nombre y luego
// la función que lo modifica.
// Luego lo voy a agregar a las props.

const [carrito, agregarProducto] = useState([]);

  return (
    <Fragment>
    <Header
    titulo = "Carrito de Compras"
    />
    
     <h1>Hola {universidad} </h1>
     {mensaje}

<h1>Listado de productos</h1>
{productos.map( producto => (
 <Producto
    key= {producto.id}
    producto={producto}
    productos={productos}
    carrito={carrito}
    agregarProducto = {agregarProducto}
 /> 
))}
     <Carrito 
     carrito = {carrito}
     agregarProducto = {agregarProducto}
     />
     <Footer />
    </ Fragment>
  );
}

export default App;
