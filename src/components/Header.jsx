import React from 'react';

const Header = ({titulo}) => {
    return (
        <div> 
        <h2>{titulo}</h2>
        <h4>Soy el header de la clase introductoria</h4>
        </div>
     );
}
 
export default Header;
